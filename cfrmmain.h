#ifndef CFRMMAIN_H
#define CFRMMAIN_H

#include <QWidget>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_legenditem.h>
#include <QPointF>
#include <QVector>
#include <QMenu>
#include <math.h>
#include <qwt_symbol.h>
#include <qwt_plot_grid.h>


namespace Ui {
class CFrmMain;
}

class CFrmMain : public QWidget
{
    Q_OBJECT

public:
    explicit CFrmMain(QWidget *parent = 0);
    ~CFrmMain();


private:
    Ui::CFrmMain *ui;
    QwtPlot *plt;
    QwtPlotCurve *crvSin;
    QwtPlotCurve *crvCos;
    QwtPlotGrid *grid;
    QwtPlotLegendItem *legendItem;
    QVector<QPointF> datSin;
    QVector<QPointF> datCos;

    void initData();
};

#endif // CFRMMAIN_H
