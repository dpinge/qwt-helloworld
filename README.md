# qwt-helloworld

This is a small program designed to test Qt and Qwt on Linux embedded systems built with Buildroot.
It consists of a QwtPlot widget showing the sine and cosine functions and a QPushButton to quit.
