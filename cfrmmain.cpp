#include "cfrmmain.h"
#include "ui_cfrmmain.h"

CFrmMain::CFrmMain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CFrmMain)
{
    ui->setupUi(this);
    setWindowTitle(QApplication::applicationName());
    plt = new QwtPlot(this);

    crvSin = new QwtPlotCurve("sin");
    QPen curvePen((QColor(Qt::red)));
    crvSin->setPen(curvePen);
    crvSin->setSymbol(new QwtSymbol(QwtSymbol::Cross,QBrush(curvePen.color()),curvePen,QSize(3,3)));

    crvCos = new QwtPlotCurve("cos");
    curvePen.setColor(Qt::blue);
    crvCos->setPen(curvePen);
    crvCos->setSymbol(new QwtSymbol(QwtSymbol::Cross,QBrush(curvePen.color()),curvePen,QSize(3,3)));
    crvSin->attach(plt);
    crvCos->attach(plt);

    QPen penGrid(QBrush(Qt::gray),1,Qt::DotLine);        // same for all grids
    grid=new QwtPlotGrid;
    grid->attach(plt);
    grid->setPen(penGrid);


#if ( (QWT_VERSION & 0xFFFF00) == 0x060100)     // Qwt v6.1
    legendItem=new QwtPlotLegendItem;
    legendItem->attach(plt);
    legendItem->setAlignment(Qt::AlignLeft|Qt::AlignBottom);
#else
#error Qwt v6.1 and above required
#endif

    plt->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->verticalLayout->insertWidget(0, plt);

    initData();
    crvSin->setSamples(datSin);
    crvCos->setSamples(datCos);
    plt->setAxisAutoScale(QwtPlot::xBottom);
    plt->setAxisAutoScale(QwtPlot::yLeft);
    plt->replot();
}

CFrmMain::~CFrmMain()
{
    delete ui;
}

void CFrmMain::initData()
{
    QPointF pt;
    double x=0.0;

    for(int i=0 ; i<30 ; i++)
    {
        pt.setX(x);
        pt.setY(sin(x));
        datSin.append(pt);
        pt.setY(cos(x));
        datCos.append(pt);
        x += 2*M_PI / 29.0;
    }
}
